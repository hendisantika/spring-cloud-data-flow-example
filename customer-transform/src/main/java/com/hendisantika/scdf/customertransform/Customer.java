package com.hendisantika.scdf.customertransform;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-cloud-data-flow-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-11
 * Time: 15:05
 */
public class Customer {
    private Long id;

    private String name;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("customer_name")
    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
