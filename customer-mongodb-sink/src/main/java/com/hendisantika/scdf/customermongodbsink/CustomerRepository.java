package com.hendisantika.scdf.customermongodbsink;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-cloud-data-flow-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-11
 * Time: 15:12
 */
@Repository
public interface CustomerRepository extends MongoRepository<Customer, Long> {
}